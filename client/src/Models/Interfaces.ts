// Интерфейс: Рейс
export interface IFlight {
    id:            number
    depDate:       string
    travelTime:    number
    depAirportID:  number
    destAirportID: number
    airplaneID:    number
}

// Интерфейс: Самолёт
export interface IAirplane {
    id:   number
    name: string
    cap:  number
}

// Интерфейс: Аэропорт
export interface IAirport {
    id:   number
    name: string
}