import { IAirplane, IAirport, IFlight } from "./Models/Interfaces"

export function getAirports(): IAirport[] {
    return [
        {
            "id": 1,
            "name": "Иркутск"
        },
        {
            "id": 2,
            "name": "Екатеринбург (Кольцово)"
        },
        {
            "id": 3,
            "name": "Красноярск (Емельяново)"
        },
        {
            "id": 4,
            "name": "Москва (Домодедово)"
        },
        {
            "id": 5,
            "name": "Волгоград (Гумрак)"
        },
        {
            "id": 6,
            "name": "Ростов-на-Дону (Платов)Ростов-на-Дону (Платов)"
        }
    ]
}

export function getAirplanes(): IAirplane[] {
    return [
        {
            "id": 1,
            "name": "Superjet 100 (RRJ-95)",
            "cap": 87
        },
        {
            "id": 2,
            "name": "Airbus A319",
            "cap": 128
        },
        {
            "id": 3,
            "name": "Аirbus A320",
            "cap": 168
        },
        {
            "id": 4,
            "name": "Boeing 737-800",
            "cap": 168
        },
        {
            "id": 5,
            "name": "Boeing 777-300/-300ER",
            "cap": 373
        },
        {
            "id": 6,
            "name": "Boeing 747-400",
            "cap": 447
        }
    ]
}
