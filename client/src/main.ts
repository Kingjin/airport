import { Modal } from 'bootstrap';
import { IAirplane, IAirport, IFlight } from "./Models/Interfaces"
import * as REST from "./REST"
import { getAirplanes, getAirports } from "./data"


let flights:   IFlight[]   = [];  // рейсы
let airplanes: IAirplane[] = []; //getAirplanes();  // самолёты
let airports:  IAirport[]  = []; //getAirports();   // аэропорты

//update_tabFlights_table();
//update_tabAirplanes_table();
//update_tabAirports_table();

//update_modalFlight_select_depAirport();
//update_modalFlight_select_destAirport();
//update_modalFlight_select_airplane();

// Элементы вкладок
const tabFlights_button_new   = document.getElementById("tabFlights_button_new");
const tabAirplanes_button_new = document.getElementById("tabAirplanes_button_new");
const tabAirports_button_new  = document.getElementById("tabAirports_button_new");

// Формы (модальные окна)
const modalFlight   = new Modal(document.getElementById("modalFlight"));
const modalAirplane = new Modal(document.getElementById("modalAirplane"));
const modalAirport  = new Modal(document.getElementById("modalAirport"));

// ID выбранных записей для редактирования
let editFlightID   = 0;
let editAirplaneID = 0;
let editAirportID  = 0;

// Элементы ввода формы "Рейс"
const modalFlight_input_depDate      = document.getElementById("modalFlight_input_depDate") as HTMLInputElement;
const modalFlight_input_travelTime   = document.getElementById("modalFlight_input_travelTime") as HTMLInputElement;
const modalFlight_select_depAirport  = document.getElementById("modalFlight_select_depAirport") as HTMLSelectElement;
const modalFlight_select_destAirport = document.getElementById("modalFlight_select_destAirport") as HTMLSelectElement;
const modalFlight_select_airplane    = document.getElementById("modalFlight_select_airplane") as HTMLSelectElement;
const modalFlight_button_save        = document.getElementById("modalFlight_button_save");

// Элементы ввода формы "Самолёт"
const modalAirplane_input_name  = document.getElementById("modalAirplane_input_name") as HTMLInputElement;
const modalAirplane_input_cap   = document.getElementById("modalAirplane_input_cap")  as HTMLInputElement;
const modalAirplane_button_save = document.getElementById("modalAirplane_button_save");

// Элементы ввода формы "Аэропорт"
const modalAirport_input_name  = document.getElementById("modalAirport_input_name") as HTMLInputElement;
const modalAirport_button_save = document.getElementById("modalAirport_button_save");


function loadFlights() {
    return REST.GET("/flights").then((data: IFlight[]) => {
        console.log(data);
        flights = data;

        update_tabFlights_table();
    })
}

function loadAirplanes() {
    return REST.GET("/airplanes").then((data: IAirplane[]) => {
        console.log(data);
        airplanes = data;

        update_tabAirplanes_table();
        update_modalFlight_select_airplane();
    })
}

function loadAirports() {
    return REST.GET("/airports").then((data: IAirport[]) => {
        console.log(data);
        airports = data;

        update_tabAirports_table();
        update_modalFlight_select_depAirport();
        update_modalFlight_select_destAirport();
    })
}

Promise.all([
    loadAirplanes(),
    loadAirports()
]).then(() => {
    loadFlights();
});


tabFlights_button_new.addEventListener("click", () => {
    showModalFlight();
});

tabAirplanes_button_new.addEventListener("click", () => {
    showModalAirplane();
});

tabAirports_button_new.addEventListener("click", () => {
    showModalAirport();
});

/** Открыть форму "Рейс" */
function showModalFlight(id: number = undefined) {
    let flight: IFlight = null;
    if (id) {
        flight = flights.find(item => item.id === id)
    }

    if (flight) {
        editFlightID = flight.id;
        modalFlight_input_depDate.value      = flight.depDate;
        modalFlight_input_travelTime.value   = String(flight.travelTime);
        modalFlight_select_depAirport.value  = String(flight.depAirportID);
        modalFlight_select_destAirport.value = String(flight.destAirportID);
        modalFlight_select_airplane.value    = String(flight.airplaneID);
    } else {
        editFlightID = 0;
        modalFlight_input_depDate.value      = "";
        modalFlight_input_travelTime.value   = "";
        modalFlight_select_depAirport.value  = "";
        modalFlight_select_destAirport.value = "";
        modalFlight_select_airplane.value    = "";
    }

    modalFlight.show();
}

/** Открыть форму "Самолёт" */
function showModalAirplane(id: number = undefined) {
    let airplane: IAirplane = null;
    if (id) {
        airplane = airplanes.find(item => item.id === id)
    }

    if (airplane) {
        editAirplaneID = id;
        modalAirplane_input_name.value = airplane.name
        modalAirplane_input_cap.value  = String(airplane.cap)
    } else {
        editAirplaneID = 0;
        modalAirplane_input_name.value = ""
        modalAirplane_input_cap.value  = ""
    }

    modalAirplane.show();
}

/** Открыть форму "Аэропорт" */
function showModalAirport(id: number = undefined) {
    let airport: IAirport = null;
    if (id) {
        airport = airports.find(item => item.id === id)
    }

    if (airport) {
        editAirportID = id;
        modalAirport_input_name.value = airport.name
    } else {
        editAirportID = 0;
        modalAirport_input_name.value = ""
    }

    modalAirport.show();
}

/** Обновление списка аэропортов вылета на форме рейса */
function update_modalFlight_select_depAirport() {
    const df = document.createDocumentFragment();

    for (let i = 0; i < airports.length; i++) {
        const option = document.createElement("option") as HTMLOptionElement;
        option.value = `${ i + 1 }`;
        option.innerHTML = airports[i].name;
        df.appendChild(option);
    }

    const select = document.getElementById("modalFlight_select_depAirport");
    select.innerHTML = "";
    select.appendChild(df);
}

/** Обновление списка аэропортов назначения на форме рейса */
function update_modalFlight_select_destAirport() {
    const df = document.createDocumentFragment();

    for (let i = 0; i < airports.length; i++) {
        const option = document.createElement("option") as HTMLOptionElement;
        option.value = `${ i + 1 }`;
        option.innerHTML = airports[i].name;
        df.appendChild(option);
    }

    const select = document.getElementById("modalFlight_select_destAirport");
    select.innerHTML = "";
    select.appendChild(df);
}

/** Обновление списка самолётов на форме рейса */
function update_modalFlight_select_airplane() {
    const df = document.createDocumentFragment();

    for (let i = 0; i < airplanes.length; i++) {
        const option = document.createElement("option") as HTMLOptionElement;
        option.value = `${ i + 1 }`;
        option.innerHTML = airplanes[i].name;
        df.appendChild(option);
    }

    const select = document.getElementById("modalFlight_select_airplane");
    select.innerHTML = "";
    select.appendChild(df);
}

/** Действия кнопок "Редактировать" и "Удалить" */
function rowAction(actionName: string, tableName: string, id: number) {
    return (ev: Event) => {
        switch (tableName) {
            case "flights":
                console.log(`${ tableName }: ${ actionName } id = ${ id }`);

                switch (actionName) {
                    case "edit":
                        showModalFlight(id);
                        break;
                    case "delete":
                        // удаление
                        break;
                }
                break;
            case "airplanes":
                console.log(`${ tableName }: ${ actionName } id = ${ id }`);

                switch (actionName) {
                    case "edit":
                        showModalAirplane(id);
                        break;
                    case "delete":
                        // удаление
                        break;
                }
                break;
            case "airports":
                console.log(`${ tableName }: ${ actionName } id = ${ id }`);

                switch (actionName) {
                    case "edit":
                        showModalAirport(id);
                        break;
                    case "delete":
                        // удаление
                        break;
                }
                break;
        }
    }
}

// Создание кнопок действий ("Редактировать" и "Удалить") для строк
function createActionButtons(tableName: string, id: number): DocumentFragment {
    const df = document.createDocumentFragment();
    const btnEdit = document.createElement("button");
    btnEdit.className = "btn btn-warning btn-sm me-2";
    btnEdit.innerHTML = /*html*/`<img src="public/img/pencil.svg" width="16" height="16" class="d-inline-block align-text-top">`;
    btnEdit.addEventListener("click", rowAction("edit", tableName, id));

    const btnDelete = document.createElement("button");
    btnDelete.className = "btn btn-danger btn-sm";
    btnDelete.innerHTML = /*html*/`<img src="public/img/trash.svg" width="16" height="16" class="d-inline-block align-text-top">`;
    btnEdit.addEventListener("click", rowAction("delete", tableName, id));

    df.appendChild(btnEdit);
    df.appendChild(btnDelete);
    return df
}

/** Обновление таблицы "Рейсы" */
function update_tabFlights_table() {
    const df = document.createDocumentFragment();

    for (const flight of flights) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");
        td.appendChild(createActionButtons("flights", flight.id));
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = `${ flight.id }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = flight.depDate;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = `${ flight.travelTime }`;
        tr.appendChild(td);

        const depAirport = airports.find(item => item.id === flight.depAirportID);
        td = document.createElement("td");
        td.innerHTML = depAirport.name;
        tr.appendChild(td);

        const destAirport = airports.find(item => item.id === flight.destAirportID);
        td = document.createElement("td");
        td.innerHTML = destAirport.name;
        tr.appendChild(td);

        const airplane = airplanes.find(item => item.id === flight.airplaneID);
        td = document.createElement("td");
        td.innerHTML = airplane.name;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_flights");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}

/** Обновление таблицы "Самолёты" */
function update_tabAirplanes_table() {
    const df = document.createDocumentFragment();

    console.log("update_tabAirplanes_table");
    console.log(airplanes);
    for (let i = 0; i < airplanes.length; i++) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");
        td.appendChild(createActionButtons("airplanes", i + 1));
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = `${ i + 1 }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = airplanes[i].name;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = `${ airplanes[i].cap }`;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_airplanes");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}

/** Обновление таблицы "Аэропорты" */
function update_tabAirports_table() {
    const df = document.createDocumentFragment();

    for (let i = 0; i < airports.length; i++) {
        const tr = document.createElement("tr");

        let td = document.createElement("td");
        td.appendChild(createActionButtons("airports", i + 1));
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = `${ i + 1 }`;
        tr.appendChild(td);

        td = document.createElement("td");
        td.innerHTML = airports[i].name;
        tr.appendChild(td);

        df.appendChild(tr);
    }

    const tbody = document.getElementById("tbody_airports");
    tbody.innerHTML = "";
    tbody.appendChild(df);
}



/** Сохранение */

modalFlight_button_save.addEventListener("click", () => {
    let flight: IFlight = {
        id:            editFlightID,
        depDate:       modalFlight_input_depDate.value,
        travelTime:    Number(modalFlight_input_travelTime.value),
        depAirportID:  Number(modalFlight_select_depAirport.value),
        destAirportID: Number(modalFlight_select_destAirport.value),
        airplaneID:    Number(modalFlight_select_airplane.value)
    }

    if (editFlightID === 0) {
        // Создание новой записи
        REST.PUT("/flights", flight).then((data: any) => {
            console.log("Рейс создан ID = ", data.id);
            loadFlights();
        })
    } else {
        // Редактирование записи
        REST.POST("/flights", flight).then((data: any) => {
            console.log("Рейс изменён");
            loadFlights();
        })
    }
});

modalAirplane_button_save.addEventListener("click", () => {
    if (editAirplaneID === 0) {
        // Создание новой записи
        airplanes.push({
            id: 0,
            name: modalAirplane_input_name.value,
            cap: Number(modalAirplane_input_cap.value)
        });
    } else {
        // Редактирование записи
        const airplane = airplanes.find(item => item.id === editAirplaneID);
        airplane.name = modalAirplane_input_name.value;
        airplane.cap = Number(modalAirplane_input_cap.value);

        // Сохранить изменения и перезагрузить данные с сервера
    }
    loadAirplanes();
});

modalAirport_button_save.addEventListener("click", () => {
    if (editAirportID === 0) {
        // Создание новой записи
        airports.push({
            id: 0,
            name: modalAirport_input_name.value
        });
    } else {
        // Редактирование записи
        const airport = airports.find(item => item.id === editAirportID);
        airport.name = modalAirport_input_name.value;

        // Сохранить изменения и перезагрузить данные с сервера
    }
    loadAirports();
});

// Выход
document.getElementById("button_logout").addEventListener("click", () => {
    REST.POST("/logout", null).then(() => {
        window.location.href = "/"
    })
});