package Routes

import (
	"Airport/Config"
	"Airport/Models"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AirplaneDTO struct {
	ID   int    `json:"id"`                      // Номер (ID)
	Name string `json:"name" binding:"required"` // Название
	Cap  int    `json:"cap"  binding:"required"` // Число мест
}

type AirplaneDTOUpdate struct {
	ID   int    `json:"id"`   // Номер (ID)
	Name string `json:"name"` // Название
	Cap  int    `json:"cap"`  // Число мест
}

// GET /airplanes
// Получаем список всех самолетов
func GetAirplanes(context *gin.Context) {
	var airplanes []Models.Airplane
	Config.DB.Preload("Flight").Find(&airplanes)

	context.JSON(http.StatusOK, airplanes)
}

// GET /airplanes/:id
// Получение одного самолета по ID
func GetAirplane(context *gin.Context) {
	// Проверяем имеется ли запись
	var airplane Models.Airplane
	if err := Config.DB.Where("id = ?", context.Param("id")).Preload("Flight").First(&airplane).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}

	context.JSON(http.StatusOK, airplane)
}

// PUT /airplanes
// Создание нового самолета
func CreateAirplane(c *gin.Context) {
	var input AirplaneDTO
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Println(c.ShouldBindJSON(&input))
	airplane := Models.Airplane{ID: input.ID, Name: input.Name, Cap: input.Cap}
	Config.DB.Create(&airplane)
	c.JSON(http.StatusOK, gin.H{"id": airplane.ID})
}

// POST /airplanes/:id
// Обновление записи самолета
func UpdateAirplane(context *gin.Context) {
	var airplane Models.Airplane
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&airplane).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}
	var input AirplaneDTOUpdate
	if err := context.ShouldBindJSON(&input); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	Config.DB.Model(&airplane).Updates(input)
	context.JSON(http.StatusOK, airplane)
}

// DELETE //airplanes/:id
// Удаление записи самолета
func DeleteAirplane(context *gin.Context) {
	var airplane Models.Airplane
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&airplane).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}
	Config.DB.Delete(&airplane)
	context.JSON(http.StatusOK, gin.H{"id": airplane.ID})
}
