package Routes

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Root - корневой маршрут
func Root(c *gin.Context) {
    session := sessions.Default(c)
	login := session.Get("user")
	if login == nil {
		c.HTML(http.StatusOK, "login.html", nil)
	} else {
		c.HTML(http.StatusOK, "main.html", nil)
	}
}
