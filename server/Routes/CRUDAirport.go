package Routes

import (
	"Airport/Config"
	"Airport/Models"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type AirportDTO struct {
	ID   int    `json:"id"`                      // Номер (ID)
	Name string `json:"name" binding:"required"` // Название
}

type AirportDTOUpdate struct {
	ID   int    `json:"id"`   // Номер (ID)
	Name string `json:"name"` // Название
}

// GET /airports
// Получение всех аэропортов
func GetAirports(context *gin.Context) {
	var airports []Models.Airport
	Config.DB.Preload("DepFlights").Preload("DestFlights").Find(&airports)

	context.JSON(http.StatusOK, airports)
}

// GET /airports/:id
// Получение одного аэропорта по ID
func GetAirport(context *gin.Context) {
	// Проверяем имеется ли запись
	var airport Models.Airport
	if err := Config.DB.Where("id = ?", context.Param("id")).Preload("DepFlights").Preload("DestFlights").First(&airport).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}

	context.JSON(http.StatusOK, airport)
}

// PUT /airports
// Создание нового аэропорта
func CreateAirport(context *gin.Context) {
	var input AirportDTO
	if err := context.ShouldBindJSON(&input); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	log.Println(context.ShouldBindJSON(&input))
	airport := Models.Airport{ID: input.ID, Name: input.Name}
	Config.DB.Create(&airport)
	context.JSON(http.StatusOK, gin.H{"id": airport.ID})
}

// POST /airports/:id
// Обновление записи аэропорта
func UpdateAirport(context *gin.Context) {
	var airport Models.Airport
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&airport).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}
	var input AirportDTOUpdate
	if err := context.ShouldBindJSON(&input); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	Config.DB.Model(&airport).Updates(input)
	context.JSON(http.StatusOK, airport)
}

// DELETE /airports/:id
// Удаление записи аэропорта
func DeleteAirport(context *gin.Context) {
	var airport Models.Airport
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&airport).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}
	Config.DB.Delete(&airport)
	context.JSON(http.StatusOK, gin.H{"id": airport.ID})
}
