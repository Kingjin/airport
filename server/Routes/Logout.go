package Routes

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Logout - выход из системы
func Logout(c *gin.Context) {
	session := sessions.Default(c)
	session.Set("user", nil)
	session.Save()
	c.JSON(200, nil)
}
