package Routes

import (
	"Airport/Config"
	"Airport/Models"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type FlightDTO struct {
	ID            int       `json:"id"`                               // Номер (ID)
	DepDate       string    `json:"depDate"       binding:"required"` // Дата и время вылета
	TravelTime    int       `json:"travelTime"    binding:"required"` // Время в пути
	DepAirportID  int       `json:"depAirportID"  binding:"required"` // ID аэропорта вылета
	DestAirportID int       `json:"destAirportID" binding:"required"` // ID аэропорта назначения
	AirplaneID    int       `json:"airplaneID"    binding:"required"` // ID самолёта
}

type FlightDTOUpdate struct {
	ID            int       `json:"id"`            // Номер (ID)
	DepDate       time.Time `json:"depDate"`       // Дата и время вылета
	TravelTime    int       `json:"travelTime"`    // Время в пути
	DepAirportID  int       `json:"depAirportID" ` // ID аэропорта вылета
	DestAirportID int       `json:"destAirportID"` // ID аэропорта назначения
	AirplaneID    int       `json:"airplaneID"`    // ID самолёта
}

// GET /flights
// Получаем список всех рейсов
func GetFlights(context *gin.Context) {
	var flights []Models.Flight
	Config.DB.Find(&flights)

	context.JSON(http.StatusOK, flights)
}

// GET /flights/:id
// Получение одного рейса по ID
func GetFlight(context *gin.Context) {
	// Проверяем имеется ли запись
	var flight Models.Flight
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&flight).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}

	context.JSON(http.StatusOK, flight)

}

// PUT /flights
// Создание нового рейса
func CreateFlight(c *gin.Context) {
	var err error
	var input FlightDTO
	if err = c.ShouldBindJSON(&input); err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}
	log.Println(c.ShouldBindJSON(&input))

	var depDate time.Time
	if depDate, err = time.Parse("2006-01-02 15:04", input.DepDate); err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}

	flight := Models.Flight{
		DepDate:       depDate,
		TravelTime:    input.TravelTime,
		DepAirportID:  input.DepAirportID,
		DestAirportID: input.DestAirportID,
		AirplaneID:    input.AirplaneID,
	}
	if err = Config.DB.Create(&flight).Error; err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, err.Error())
	} else {
		c.JSON(http.StatusOK, gin.H{"id": flight.ID})
	}
}

// POST /flights/:id
// Обновление записи рейса
func UpdateFlight(context *gin.Context) {
	var flight Models.Flight
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&flight).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}
	var input FlightDTOUpdate
	if err := context.ShouldBindJSON(&input); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	Config.DB.Model(&flight).Updates(input)
	context.JSON(http.StatusOK, flight)
}

// DELETE /flights/:id
// Удаление записи рейса
func DeleteFlight(context *gin.Context) {
	var flight Models.Flight
	if err := Config.DB.Where("id = ?", context.Param("id")).First(&flight).Error; err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": "Запись не существует"})
		return
	}
	Config.DB.Delete(&flight)
	context.JSON(http.StatusOK, gin.H{"id": flight.ID})
}
