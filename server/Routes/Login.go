package Routes

import (
	"errors"
	"log"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Login - логин
func Login(c *gin.Context) {
	var err error
	defer func() {
		if err != nil {
			c.String(http.StatusInternalServerError, err.Error())
		} else {
			c.JSON(http.StatusOK, nil)
		}
	}()

	params := struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}{}
	if err = c.ShouldBindJSON(&params); err != nil {
		log.Println(err)
		return
	}

	// Пример проверки логина и пароля
	// ! переделать на запрос из БД
    if params.Login != "user" && params.Password != "user" {
		err = errors.New("неверный логин или пароль")
		log.Println(err)
		return
	}

	session := sessions.Default(c)
	session.Set("user", params.Login)
	if err = session.Save(); err != nil {
		log.Println(err)
		return
	}

}
