package Models

import (
	"Airport/Config"
	"math/rand"
	"time"
)

func randate() time.Time {
	min := time.Date(2022, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2023, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min

	sec := rand.Int63n(delta) + min
	return time.Unix(sec, 0)
}

func DataFilling() {
	Config.DB.Create(&Airplane{ID: 1, Name: "Superjet 100 (RRJ-95)", Cap: 87})
	Config.DB.Create(&Airplane{ID: 2, Name: "Airbus A319", Cap: 128})
	Config.DB.Create(&Airplane{ID: 3, Name: "Аirbus A320", Cap: 168})
	Config.DB.Create(&Airplane{ID: 4, Name: "Boeing 737-800", Cap: 168})
	Config.DB.Create(&Airplane{ID: 5, Name: "Boeing 777-300/-300ER", Cap: 373})
	Config.DB.Create(&Airplane{ID: 6, Name: "Boeing 747-400", Cap: 447})
	Config.DB.Create(&Airport{ID: 1, Name: "Иркутск"})
	Config.DB.Create(&Airport{ID: 2, Name: "Екатеринбург (Кольцово)"})
	Config.DB.Create(&Airport{ID: 3, Name: "Красноярск (Емельяново)"})
	Config.DB.Create(&Airport{ID: 4, Name: "Москва (Домодедово)"})
	Config.DB.Create(&Airport{ID: 5, Name: "Волгоград (Гумрак)"})
	Config.DB.Create(&Airport{ID: 6, Name: "Ростов-на-Дону (Платов)"})
	Config.DB.Create(&Flight{ID: 1, DepDate: randate(), TravelTime: 80, DepAirportID: 1, DestAirportID: 3, AirplaneID: 1})
	Config.DB.Create(&Flight{ID: 2, DepDate: randate(), TravelTime: 295, DepAirportID: 3, DestAirportID: 4, AirplaneID: 3})
	Config.DB.Create(&Flight{ID: 3, DepDate: randate(), TravelTime: 170, DepAirportID: 2, DestAirportID: 3, AirplaneID: 2})
	Config.DB.Create(&Flight{ID: 4, DepDate: randate(), TravelTime: 158, DepAirportID: 4, DestAirportID: 2, AirplaneID: 6})
	Config.DB.Create(&Flight{ID: 5, DepDate: randate(), TravelTime: 105, DepAirportID: 4, DestAirportID: 5, AirplaneID: 5})
	Config.DB.Create(&Flight{ID: 6, DepDate: randate(), TravelTime: 560, DepAirportID: 1, DestAirportID: 6, AirplaneID: 4})
}
