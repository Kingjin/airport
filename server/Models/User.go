package Models

// Пользователь
type User struct {
	ID       int    `json:"id"`       // Номер (ID)
	Name     string `json:"name"`     // Ф.И.О.
	Login    string `json:"login"`    // Логин
	Password string `json:"password"` // Пароль
}