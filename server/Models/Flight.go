package Models

import "time"

// Рейс
type Flight struct {
	ID            int       `json:"id"`            // Номер (ID)
	DepDate       time.Time `json:"depDate"`       // Дата и время вылета
	TravelTime    int       `json:"travelTime"`    // Время в пути
	DepAirportID  int       `json:"depAirportID"`  // ID аэропорта вылета
	DestAirportID int       `json:"destAirportID"` // ID аэропорта назначения
	AirplaneID    int       `json:"airplaneID"`    // ID самолёта
}
