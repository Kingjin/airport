package Models

// Аэропорт
type Airport struct {
	ID   int    `json:"id"`   // Номер (ID)
	Name string `json:"name"` // Название

	// Связь "Один ко многим" с моделью Flight
	DepFlights  []Flight `gorm:"foreignKey:DepAirportID"`  // Рейсы вылета
	DestFlights []Flight `gorm:"foreignKey:DestAirportID"` // Рейсы назначения
}
