package main

import (
	"Airport/Config"
	"Airport/Models"
	"Airport/Routes"
	"log"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/memstore"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {

	var err error

	// Инициализация БД
	conn := "host=localhost user=postgres password=postgres dbname=Airport sslmode=disable"
	Config.DB, err = gorm.Open(postgres.Open(conn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	// Миграция БД
	Config.DB.AutoMigrate(
		&Models.Airport{},
		&Models.Airplane{},
		&Models.Flight{},
		&Models.User{},
	)

	//Models.DataFilling()

	// Инициализация веб-сервера
	r := gin.Default()
	r.Static("/public", "../client/public")

	r.LoadHTMLFiles("../client/public/login.html", "../client/public/main.html")

	// Подключение сессий
	store := memstore.NewStore([]byte("secret"))
	r.Use(sessions.Sessions("app", store))

	r.POST("/login", Routes.Login)
	r.POST("/logout", Routes.Logout)

	authorized := r.Group("/")
	authorized.Use(AuthRequired())
	{
		authorized.GET("/", Routes.Root)

		authorized.GET("/airports", Routes.GetAirports)          // получение всех аэропортов
		authorized.GET("/airports/:id", Routes.GetAirport)       // получение аэропорта по id
		authorized.PUT("/airports", Routes.CreateAirport)        // создание аэропорта
		authorized.POST("/airports/:id", Routes.UpdateAirport)   // Обновление записи аэропорта
		authorized.DELETE("/airports/:id", Routes.DeleteAirport) // Удаление записи аэропорта

		authorized.GET("/airplanes", Routes.GetAirplanes)          // получение всех самолетов
		authorized.GET("/airplanes/:id", Routes.GetAirplane)       // получение самолета по id
		authorized.PUT("/airplanes", Routes.CreateAirplane)        // создание самолета
		authorized.POST("/airplanes/:id", Routes.UpdateAirplane)   // Обновление записи самолета
		authorized.DELETE("/airplanes/:id", Routes.DeleteAirplane) // Удаление записи самолета

		authorized.GET("/flights", Routes.GetFlights)          // получение всех рейсов
		authorized.GET("/flights/:id", Routes.GetFlight)       // получение рейса по id
		authorized.PUT("/flights", Routes.CreateFlight)        // создание рейса
		authorized.POST("/flights/:id", Routes.UpdateFlight)   // Обновление записи рейса
		authorized.DELETE("/flights/:id", Routes.DeleteFlight) // Удаление записи рейса
	}
	// Запуск веб-сервера
	r.Run(":7000")
}

// AuthRequired - посредник (middleware) для проверки авторизации
func AuthRequired() gin.HandlerFunc {
	return func(c *gin.Context) {
		session := sessions.Default(c)
		login := session.Get("user")
		log.Println("Auth login: ", login)
		if login == nil {
			c.HTML(http.StatusOK, "login.html", nil)
			c.Abort()
			return
		}
		// Continue down the chain to handler etc
		c.Next()
	}
}
